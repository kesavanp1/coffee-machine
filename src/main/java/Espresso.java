public class Espresso implements Coffee {
    public void create(CoffeeDriver coffeeDriver) {
        coffeeDriver.dispenseHotWater();
        coffeeDriver.dispenseHotWater();
        coffeeDriver.dispenseCoffee();
    }
}