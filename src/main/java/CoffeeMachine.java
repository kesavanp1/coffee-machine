import java.util.HashMap;

public class CoffeeMachine {

    HashMap<String, Coffee> coffees = new HashMap<>();
    private CoffeeDriver coffeeDriver;

    public CoffeeMachine(CoffeeDriver coffeeDriver) {
        this.coffeeDriver = coffeeDriver;

    }

    public void addCoffee(String name, Coffee coffee) {
        coffees.put(name, coffee);
    }

    public void make(Coffee coffee) {
        coffee.create(coffeeDriver);
    }

    public void make(String coffeeName) {
        coffees.get(coffeeName).create(coffeeDriver);
    }
}
