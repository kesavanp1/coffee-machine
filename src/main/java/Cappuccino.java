public class Cappuccino implements Coffee {

    public void create(CoffeeDriver coffeeDriver) {
        coffeeDriver.dispenseMilk();
        coffeeDriver.dispenseCoffee();
        coffeeDriver.dispenseMilkFoam();
    }
}