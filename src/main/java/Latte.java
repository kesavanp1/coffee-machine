public class Latte implements Coffee{
    public void create(CoffeeDriver coffeeDriver) {
        coffeeDriver.dispenseHotWater();
        coffeeDriver.dispenseHotWater();
        coffeeDriver.dispenseCoffee();
        coffeeDriver.dispenseMilk();
        coffeeDriver.dispenseMilkFoam();
    }
}