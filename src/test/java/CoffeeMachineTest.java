import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class CoffeeMachineTest {
    CoffeeDriver coffeeDriver = mock(CoffeeDriver.class);
    CoffeeMachine coffeeMachine = new CoffeeMachine(coffeeDriver);

    @Test
    public void shouldAbleToMakeCappuccino() {
        final Cappuccino cappuccino = new Cappuccino();
        coffeeMachine.addCoffee("cappuccino", cappuccino);
        coffeeMachine.make("cappuccino");
        verify(coffeeDriver, times(1)).dispenseMilk();
        verify(coffeeDriver, times(1)).dispenseCoffee();
        verify(coffeeDriver, times(1)).dispenseMilkFoam();
    }

    @Test
    public void shouldAbleToMakeEspresso() {
        final Espresso espresso = new Espresso();
        coffeeMachine.addCoffee("espresso", espresso);
        coffeeMachine.make("espresso");
        verify(coffeeDriver, times(2)).dispenseHotWater();
        verify(coffeeDriver, times(1)).dispenseCoffee();
    }

    @Test
    public void shouldAbleToMakeLatte() {
        final Latte latte = new Latte();
        coffeeMachine.addCoffee("latte", latte);
        coffeeMachine.make("latte");
        verify(coffeeDriver, times(2)).dispenseHotWater();
        verify(coffeeDriver, times(1)).dispenseCoffee();
        verify(coffeeDriver, times(1)).dispenseMilk();
        verify(coffeeDriver, times(1)).dispenseMilkFoam();
    }

    @Test
    public void shouldAbleToCreateCustomCoffee() {
        class Mocha implements Coffee {

            @Override
            public void create(CoffeeDriver coffeeDriver) {
                coffeeDriver.dispenseCoffee();
                coffeeDriver.dispenseMilk();
                coffeeDriver.dispenseCream();
                coffeeDriver.dispenseCream();
            }
        }
        coffeeMachine.make(new Mocha());
        verify(coffeeDriver, times(1)).dispenseCoffee();
        verify(coffeeDriver, times(1)).dispenseMilk();
        verify(coffeeDriver, times(2)).dispenseCream();
    }
}
